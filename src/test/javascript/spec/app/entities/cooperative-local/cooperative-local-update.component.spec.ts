/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import CooperativeLocalUpdateComponent from '@/entities/cooperative-local/cooperative-local-update.vue';
import CooperativeLocalClass from '@/entities/cooperative-local/cooperative-local-update.component';
import CooperativeLocalService from '@/entities/cooperative-local/cooperative-local.service';

import RestaurantService from '@/entities/restaurant/restaurant.service';

import CooperativeService from '@/entities/cooperative/cooperative.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('CooperativeLocal Management Update Component', () => {
    let wrapper: Wrapper<CooperativeLocalClass>;
    let comp: CooperativeLocalClass;
    let cooperativeLocalServiceStub: SinonStubbedInstance<CooperativeLocalService>;

    beforeEach(() => {
      cooperativeLocalServiceStub = sinon.createStubInstance<CooperativeLocalService>(CooperativeLocalService);

      wrapper = shallowMount<CooperativeLocalClass>(CooperativeLocalUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          cooperativeLocalService: () => cooperativeLocalServiceStub,
          alertService: () => new AlertService(),

          restaurantService: () =>
            sinon.createStubInstance<RestaurantService>(RestaurantService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          cooperativeService: () =>
            sinon.createStubInstance<CooperativeService>(CooperativeService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.cooperativeLocal = entity;
        cooperativeLocalServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cooperativeLocalServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.cooperativeLocal = entity;
        cooperativeLocalServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(cooperativeLocalServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundCooperativeLocal = { id: 123 };
        cooperativeLocalServiceStub.find.resolves(foundCooperativeLocal);
        cooperativeLocalServiceStub.retrieve.resolves([foundCooperativeLocal]);

        // WHEN
        comp.beforeRouteEnter({ params: { cooperativeLocalId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.cooperativeLocal).toBe(foundCooperativeLocal);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
