/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ElementPanierComponent from '@/entities/element-panier/element-panier.vue';
import ElementPanierClass from '@/entities/element-panier/element-panier.component';
import ElementPanierService from '@/entities/element-panier/element-panier.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(ToastPlugin);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-badge', {});
localVue.component('jhi-sort-indicator', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('ElementPanier Management Component', () => {
    let wrapper: Wrapper<ElementPanierClass>;
    let comp: ElementPanierClass;
    let elementPanierServiceStub: SinonStubbedInstance<ElementPanierService>;

    beforeEach(() => {
      elementPanierServiceStub = sinon.createStubInstance<ElementPanierService>(ElementPanierService);
      elementPanierServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<ElementPanierClass>(ElementPanierComponent, {
        store,
        i18n,
        localVue,
        stubs: { jhiItemCount: true, bPagination: true, bModal: bModalStub as any },
        provide: {
          elementPanierService: () => elementPanierServiceStub,
          alertService: () => new AlertService(),
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      elementPanierServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.retrieveAllElementPaniers();
      await comp.$nextTick();

      // THEN
      expect(elementPanierServiceStub.retrieve.called).toBeTruthy();
      expect(comp.elementPaniers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should load a page', async () => {
      // GIVEN
      elementPanierServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();

      // THEN
      expect(elementPanierServiceStub.retrieve.called).toBeTruthy();
      expect(comp.elementPaniers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should not load a page if the page is the same as the previous page', () => {
      // GIVEN
      elementPanierServiceStub.retrieve.reset();
      comp.previousPage = 1;

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(elementPanierServiceStub.retrieve.called).toBeFalsy();
    });

    it('should re-initialize the page', async () => {
      // GIVEN
      elementPanierServiceStub.retrieve.reset();
      elementPanierServiceStub.retrieve.resolves({ headers: {}, data: [{ id: 123 }] });

      // WHEN
      comp.loadPage(2);
      await comp.$nextTick();
      comp.clear();
      await comp.$nextTick();

      // THEN
      expect(elementPanierServiceStub.retrieve.callCount).toEqual(3);
      expect(comp.page).toEqual(1);
      expect(comp.elementPaniers[0]).toEqual(expect.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // GIVEN
      comp.propOrder = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      elementPanierServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: 123 });
      expect(elementPanierServiceStub.retrieve.callCount).toEqual(1);

      comp.removeElementPanier();
      await comp.$nextTick();

      // THEN
      expect(elementPanierServiceStub.delete.called).toBeTruthy();
      expect(elementPanierServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
