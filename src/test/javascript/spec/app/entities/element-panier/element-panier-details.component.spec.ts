/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ElementPanierDetailComponent from '@/entities/element-panier/element-panier-details.vue';
import ElementPanierClass from '@/entities/element-panier/element-panier-details.component';
import ElementPanierService from '@/entities/element-panier/element-panier.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ElementPanier Management Detail Component', () => {
    let wrapper: Wrapper<ElementPanierClass>;
    let comp: ElementPanierClass;
    let elementPanierServiceStub: SinonStubbedInstance<ElementPanierService>;

    beforeEach(() => {
      elementPanierServiceStub = sinon.createStubInstance<ElementPanierService>(ElementPanierService);

      wrapper = shallowMount<ElementPanierClass>(ElementPanierDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { elementPanierService: () => elementPanierServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundElementPanier = { id: 123 };
        elementPanierServiceStub.find.resolves(foundElementPanier);

        // WHEN
        comp.retrieveElementPanier(123);
        await comp.$nextTick();

        // THEN
        expect(comp.elementPanier).toBe(foundElementPanier);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundElementPanier = { id: 123 };
        elementPanierServiceStub.find.resolves(foundElementPanier);

        // WHEN
        comp.beforeRouteEnter({ params: { elementPanierId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.elementPanier).toBe(foundElementPanier);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
