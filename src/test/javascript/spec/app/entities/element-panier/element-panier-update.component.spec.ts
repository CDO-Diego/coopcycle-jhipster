/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import ElementPanierUpdateComponent from '@/entities/element-panier/element-panier-update.vue';
import ElementPanierClass from '@/entities/element-panier/element-panier-update.component';
import ElementPanierService from '@/entities/element-panier/element-panier.service';

import PanierService from '@/entities/panier/panier.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ElementPanier Management Update Component', () => {
    let wrapper: Wrapper<ElementPanierClass>;
    let comp: ElementPanierClass;
    let elementPanierServiceStub: SinonStubbedInstance<ElementPanierService>;

    beforeEach(() => {
      elementPanierServiceStub = sinon.createStubInstance<ElementPanierService>(ElementPanierService);

      wrapper = shallowMount<ElementPanierClass>(ElementPanierUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          elementPanierService: () => elementPanierServiceStub,
          alertService: () => new AlertService(),

          panierService: () =>
            sinon.createStubInstance<PanierService>(PanierService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.elementPanier = entity;
        elementPanierServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(elementPanierServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.elementPanier = entity;
        elementPanierServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(elementPanierServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundElementPanier = { id: 123 };
        elementPanierServiceStub.find.resolves(foundElementPanier);
        elementPanierServiceStub.retrieve.resolves([foundElementPanier]);

        // WHEN
        comp.beforeRouteEnter({ params: { elementPanierId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.elementPanier).toBe(foundElementPanier);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
