/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import * as config from '@/shared/config/config';
import MembreUpdateComponent from '@/entities/membre/membre-update.vue';
import MembreClass from '@/entities/membre/membre-update.component';
import MembreService from '@/entities/membre/membre.service';

import CooperativeService from '@/entities/cooperative/cooperative.service';

import PanierService from '@/entities/panier/panier.service';

import LivraisonService from '@/entities/livraison/livraison.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Membre Management Update Component', () => {
    let wrapper: Wrapper<MembreClass>;
    let comp: MembreClass;
    let membreServiceStub: SinonStubbedInstance<MembreService>;

    beforeEach(() => {
      membreServiceStub = sinon.createStubInstance<MembreService>(MembreService);

      wrapper = shallowMount<MembreClass>(MembreUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          membreService: () => membreServiceStub,
          alertService: () => new AlertService(),

          cooperativeService: () =>
            sinon.createStubInstance<CooperativeService>(CooperativeService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          panierService: () =>
            sinon.createStubInstance<PanierService>(PanierService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          livraisonService: () =>
            sinon.createStubInstance<LivraisonService>(LivraisonService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.membre = entity;
        membreServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(membreServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.membre = entity;
        membreServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(membreServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundMembre = { id: 123 };
        membreServiceStub.find.resolves(foundMembre);
        membreServiceStub.retrieve.resolves([foundMembre]);

        // WHEN
        comp.beforeRouteEnter({ params: { membreId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.membre).toBe(foundMembre);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
