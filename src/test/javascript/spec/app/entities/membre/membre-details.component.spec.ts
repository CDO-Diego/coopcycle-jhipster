/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import MembreDetailComponent from '@/entities/membre/membre-details.vue';
import MembreClass from '@/entities/membre/membre-details.component';
import MembreService from '@/entities/membre/membre.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Membre Management Detail Component', () => {
    let wrapper: Wrapper<MembreClass>;
    let comp: MembreClass;
    let membreServiceStub: SinonStubbedInstance<MembreService>;

    beforeEach(() => {
      membreServiceStub = sinon.createStubInstance<MembreService>(MembreService);

      wrapper = shallowMount<MembreClass>(MembreDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { membreService: () => membreServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundMembre = { id: 123 };
        membreServiceStub.find.resolves(foundMembre);

        // WHEN
        comp.retrieveMembre(123);
        await comp.$nextTick();

        // THEN
        expect(comp.membre).toBe(foundMembre);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundMembre = { id: 123 };
        membreServiceStub.find.resolves(foundMembre);

        // WHEN
        comp.beforeRouteEnter({ params: { membreId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.membre).toBe(foundMembre);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
