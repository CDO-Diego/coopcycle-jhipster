import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('CooperativeLocal e2e test', () => {
  const cooperativeLocalPageUrl = '/cooperative-local';
  const cooperativeLocalPageUrlPattern = new RegExp('/cooperative-local(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const cooperativeLocalSample = { nom: 'Dollar', localisation: 'Card compress' };

  let cooperativeLocal;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/cooperative-locals+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/cooperative-locals').as('postEntityRequest');
    cy.intercept('DELETE', '/api/cooperative-locals/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (cooperativeLocal) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/cooperative-locals/${cooperativeLocal.id}`,
      }).then(() => {
        cooperativeLocal = undefined;
      });
    }
  });

  it('CooperativeLocals menu should load CooperativeLocals page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('cooperative-local');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('CooperativeLocal').should('exist');
    cy.url().should('match', cooperativeLocalPageUrlPattern);
  });

  describe('CooperativeLocal page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(cooperativeLocalPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create CooperativeLocal page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/cooperative-local/new$'));
        cy.getEntityCreateUpdateHeading('CooperativeLocal');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', cooperativeLocalPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/cooperative-locals',
          body: cooperativeLocalSample,
        }).then(({ body }) => {
          cooperativeLocal = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/cooperative-locals+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/cooperative-locals?page=0&size=20>; rel="last",<http://localhost/api/cooperative-locals?page=0&size=20>; rel="first"',
              },
              body: [cooperativeLocal],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(cooperativeLocalPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details CooperativeLocal page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('cooperativeLocal');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', cooperativeLocalPageUrlPattern);
      });

      it('edit button click should load edit CooperativeLocal page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('CooperativeLocal');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', cooperativeLocalPageUrlPattern);
      });

      it('edit button click should load edit CooperativeLocal page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('CooperativeLocal');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', cooperativeLocalPageUrlPattern);
      });

      it('last delete button click should delete instance of CooperativeLocal', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('cooperativeLocal').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', cooperativeLocalPageUrlPattern);

        cooperativeLocal = undefined;
      });
    });
  });

  describe('new CooperativeLocal page', () => {
    beforeEach(() => {
      cy.visit(`${cooperativeLocalPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('CooperativeLocal');
    });

    it('should create an instance of CooperativeLocal', () => {
      cy.get(`[data-cy="nom"]`).type('functionalities JSON').should('have.value', 'functionalities JSON');

      cy.get(`[data-cy="localisation"]`).type('out-of-the-box').should('have.value', 'out-of-the-box');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        cooperativeLocal = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', cooperativeLocalPageUrlPattern);
    });
  });
});
