import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Membre e2e test', () => {
  const membrePageUrl = '/membre';
  const membrePageUrlPattern = new RegExp('/membre(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const membreSample = { nom: 'États-Unis', prenom: 'RSS', type: 'LIVREUR', adresse: 'Dahomey transmitter' };

  let membre;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/membres+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/membres').as('postEntityRequest');
    cy.intercept('DELETE', '/api/membres/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (membre) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/membres/${membre.id}`,
      }).then(() => {
        membre = undefined;
      });
    }
  });

  it('Membres menu should load Membres page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('membre');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Membre').should('exist');
    cy.url().should('match', membrePageUrlPattern);
  });

  describe('Membre page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(membrePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Membre page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/membre/new$'));
        cy.getEntityCreateUpdateHeading('Membre');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', membrePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/membres',
          body: membreSample,
        }).then(({ body }) => {
          membre = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/membres+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/membres?page=0&size=20>; rel="last",<http://localhost/api/membres?page=0&size=20>; rel="first"',
              },
              body: [membre],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(membrePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Membre page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('membre');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', membrePageUrlPattern);
      });

      it('edit button click should load edit Membre page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Membre');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', membrePageUrlPattern);
      });

      it('edit button click should load edit Membre page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Membre');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', membrePageUrlPattern);
      });

      it('last delete button click should delete instance of Membre', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('membre').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', membrePageUrlPattern);

        membre = undefined;
      });
    });
  });

  describe('new Membre page', () => {
    beforeEach(() => {
      cy.visit(`${membrePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Membre');
    });

    it('should create an instance of Membre', () => {
      cy.get(`[data-cy="nom"]`).type('c web-enabled core').should('have.value', 'c web-enabled core');

      cy.get(`[data-cy="prenom"]`).type('efficient bluetooth Health').should('have.value', 'efficient bluetooth Health');

      cy.get(`[data-cy="type"]`).select('LIVREUR');

      cy.get(`[data-cy="adresse"]`).type('implement').should('have.value', 'implement');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        membre = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', membrePageUrlPattern);
    });
  });
});
