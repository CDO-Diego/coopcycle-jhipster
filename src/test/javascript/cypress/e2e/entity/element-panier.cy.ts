import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('ElementPanier e2e test', () => {
  const elementPanierPageUrl = '/element-panier';
  const elementPanierPageUrlPattern = new RegExp('/element-panier(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const elementPanierSample = { nom: 'Pants architect', prix: 48729, quantity: 51412 };

  let elementPanier;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/element-paniers+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/element-paniers').as('postEntityRequest');
    cy.intercept('DELETE', '/api/element-paniers/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (elementPanier) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/element-paniers/${elementPanier.id}`,
      }).then(() => {
        elementPanier = undefined;
      });
    }
  });

  it('ElementPaniers menu should load ElementPaniers page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('element-panier');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('ElementPanier').should('exist');
    cy.url().should('match', elementPanierPageUrlPattern);
  });

  describe('ElementPanier page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(elementPanierPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create ElementPanier page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/element-panier/new$'));
        cy.getEntityCreateUpdateHeading('ElementPanier');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', elementPanierPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/element-paniers',
          body: elementPanierSample,
        }).then(({ body }) => {
          elementPanier = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/element-paniers+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/element-paniers?page=0&size=20>; rel="last",<http://localhost/api/element-paniers?page=0&size=20>; rel="first"',
              },
              body: [elementPanier],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(elementPanierPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details ElementPanier page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('elementPanier');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', elementPanierPageUrlPattern);
      });

      it('edit button click should load edit ElementPanier page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('ElementPanier');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', elementPanierPageUrlPattern);
      });

      it('edit button click should load edit ElementPanier page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('ElementPanier');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', elementPanierPageUrlPattern);
      });

      it('last delete button click should delete instance of ElementPanier', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('elementPanier').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', elementPanierPageUrlPattern);

        elementPanier = undefined;
      });
    });
  });

  describe('new ElementPanier page', () => {
    beforeEach(() => {
      cy.visit(`${elementPanierPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('ElementPanier');
    });

    it('should create an instance of ElementPanier', () => {
      cy.get(`[data-cy="nom"]`).type('quantify world-class').should('have.value', 'quantify world-class');

      cy.get(`[data-cy="prix"]`).type('50187').should('have.value', '50187');

      cy.get(`[data-cy="quantity"]`).type('42055').should('have.value', '42055');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        elementPanier = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', elementPanierPageUrlPattern);
    });
  });
});
