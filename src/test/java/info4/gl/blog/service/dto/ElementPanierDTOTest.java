package info4.gl.blog.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import info4.gl.blog.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ElementPanierDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ElementPanierDTO.class);
        ElementPanierDTO elementPanierDTO1 = new ElementPanierDTO();
        elementPanierDTO1.setId(1L);
        ElementPanierDTO elementPanierDTO2 = new ElementPanierDTO();
        assertThat(elementPanierDTO1).isNotEqualTo(elementPanierDTO2);
        elementPanierDTO2.setId(elementPanierDTO1.getId());
        assertThat(elementPanierDTO1).isEqualTo(elementPanierDTO2);
        elementPanierDTO2.setId(2L);
        assertThat(elementPanierDTO1).isNotEqualTo(elementPanierDTO2);
        elementPanierDTO1.setId(null);
        assertThat(elementPanierDTO1).isNotEqualTo(elementPanierDTO2);
    }
}
