package info4.gl.blog.domain;

import static org.assertj.core.api.Assertions.assertThat;

import info4.gl.blog.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ElementPanierTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ElementPanier.class);
        ElementPanier elementPanier1 = new ElementPanier();
        elementPanier1.setId(1L);
        ElementPanier elementPanier2 = new ElementPanier();
        elementPanier2.setId(elementPanier1.getId());
        assertThat(elementPanier1).isEqualTo(elementPanier2);
        elementPanier2.setId(2L);
        assertThat(elementPanier1).isNotEqualTo(elementPanier2);
        elementPanier1.setId(null);
        assertThat(elementPanier1).isNotEqualTo(elementPanier2);
    }
}
