package info4.gl.blog.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import info4.gl.blog.IntegrationTest;
import info4.gl.blog.domain.Membre;
import info4.gl.blog.domain.enumeration.MembreType;
import info4.gl.blog.repository.MembreRepository;
import info4.gl.blog.service.dto.MembreDTO;
import info4.gl.blog.service.mapper.MembreMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MembreResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MembreResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final MembreType DEFAULT_TYPE = MembreType.ARTISANT;
    private static final MembreType UPDATED_TYPE = MembreType.LIVREUR;

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/membres";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MembreRepository membreRepository;

    @Autowired
    private MembreMapper membreMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMembreMockMvc;

    private Membre membre;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Membre createEntity(EntityManager em) {
        Membre membre = new Membre().nom(DEFAULT_NOM).prenom(DEFAULT_PRENOM).type(DEFAULT_TYPE).adresse(DEFAULT_ADRESSE);
        return membre;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Membre createUpdatedEntity(EntityManager em) {
        Membre membre = new Membre().nom(UPDATED_NOM).prenom(UPDATED_PRENOM).type(UPDATED_TYPE).adresse(UPDATED_ADRESSE);
        return membre;
    }

    @BeforeEach
    public void initTest() {
        membre = createEntity(em);
    }

    @Test
    @Transactional
    void createMembre() throws Exception {
        int databaseSizeBeforeCreate = membreRepository.findAll().size();
        // Create the Membre
        MembreDTO membreDTO = membreMapper.toDto(membre);
        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreDTO)))
            .andExpect(status().isCreated());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeCreate + 1);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testMembre.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testMembre.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
    }

    @Test
    @Transactional
    void createMembreWithExistingId() throws Exception {
        // Create the Membre with an existing ID
        membre.setId(1L);
        MembreDTO membreDTO = membreMapper.toDto(membre);

        int databaseSizeBeforeCreate = membreRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setNom(null);

        // Create the Membre, which fails.
        MembreDTO membreDTO = membreMapper.toDto(membre);

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreDTO)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPrenomIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setPrenom(null);

        // Create the Membre, which fails.
        MembreDTO membreDTO = membreMapper.toDto(membre);

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreDTO)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setType(null);

        // Create the Membre, which fails.
        MembreDTO membreDTO = membreMapper.toDto(membre);

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreDTO)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkAdresseIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setAdresse(null);

        // Create the Membre, which fails.
        MembreDTO membreDTO = membreMapper.toDto(membre);

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreDTO)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMembres() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        // Get all the membreList
        restMembreMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(membre.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)));
    }

    @Test
    @Transactional
    void getMembre() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        // Get the membre
        restMembreMockMvc
            .perform(get(ENTITY_API_URL_ID, membre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(membre.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE));
    }

    @Test
    @Transactional
    void getNonExistingMembre() throws Exception {
        // Get the membre
        restMembreMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingMembre() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeUpdate = membreRepository.findAll().size();

        // Update the membre
        Membre updatedMembre = membreRepository.findById(membre.getId()).get();
        // Disconnect from session so that the updates on updatedMembre are not directly saved in db
        em.detach(updatedMembre);
        updatedMembre.nom(UPDATED_NOM).prenom(UPDATED_PRENOM).type(UPDATED_TYPE).adresse(UPDATED_ADRESSE);
        MembreDTO membreDTO = membreMapper.toDto(updatedMembre);

        restMembreMockMvc
            .perform(
                put(ENTITY_API_URL_ID, membreDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membreDTO))
            )
            .andExpect(status().isOk());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testMembre.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testMembre.getAdresse()).isEqualTo(UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    void putNonExistingMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // Create the Membre
        MembreDTO membreDTO = membreMapper.toDto(membre);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                put(ENTITY_API_URL_ID, membreDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membreDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // Create the Membre
        MembreDTO membreDTO = membreMapper.toDto(membre);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membreDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // Create the Membre
        MembreDTO membreDTO = membreMapper.toDto(membre);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMembreWithPatch() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeUpdate = membreRepository.findAll().size();

        // Update the membre using partial update
        Membre partialUpdatedMembre = new Membre();
        partialUpdatedMembre.setId(membre.getId());

        partialUpdatedMembre.nom(UPDATED_NOM);

        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembre.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembre))
            )
            .andExpect(status().isOk());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testMembre.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testMembre.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
    }

    @Test
    @Transactional
    void fullUpdateMembreWithPatch() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeUpdate = membreRepository.findAll().size();

        // Update the membre using partial update
        Membre partialUpdatedMembre = new Membre();
        partialUpdatedMembre.setId(membre.getId());

        partialUpdatedMembre.nom(UPDATED_NOM).prenom(UPDATED_PRENOM).type(UPDATED_TYPE).adresse(UPDATED_ADRESSE);

        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembre.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembre))
            )
            .andExpect(status().isOk());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testMembre.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testMembre.getAdresse()).isEqualTo(UPDATED_ADRESSE);
    }

    @Test
    @Transactional
    void patchNonExistingMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // Create the Membre
        MembreDTO membreDTO = membreMapper.toDto(membre);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, membreDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membreDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // Create the Membre
        MembreDTO membreDTO = membreMapper.toDto(membre);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membreDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // Create the Membre
        MembreDTO membreDTO = membreMapper.toDto(membre);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(membreDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMembre() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeDelete = membreRepository.findAll().size();

        // Delete the membre
        restMembreMockMvc
            .perform(delete(ENTITY_API_URL_ID, membre.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
