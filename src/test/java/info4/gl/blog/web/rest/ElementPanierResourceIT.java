package info4.gl.blog.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import info4.gl.blog.IntegrationTest;
import info4.gl.blog.domain.ElementPanier;
import info4.gl.blog.repository.ElementPanierRepository;
import info4.gl.blog.service.dto.ElementPanierDTO;
import info4.gl.blog.service.mapper.ElementPanierMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ElementPanierResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ElementPanierResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final Float DEFAULT_PRIX = 1F;
    private static final Float UPDATED_PRIX = 2F;

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;

    private static final String ENTITY_API_URL = "/api/element-paniers";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ElementPanierRepository elementPanierRepository;

    @Autowired
    private ElementPanierMapper elementPanierMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restElementPanierMockMvc;

    private ElementPanier elementPanier;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ElementPanier createEntity(EntityManager em) {
        ElementPanier elementPanier = new ElementPanier().nom(DEFAULT_NOM).prix(DEFAULT_PRIX).quantity(DEFAULT_QUANTITY);
        return elementPanier;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ElementPanier createUpdatedEntity(EntityManager em) {
        ElementPanier elementPanier = new ElementPanier().nom(UPDATED_NOM).prix(UPDATED_PRIX).quantity(UPDATED_QUANTITY);
        return elementPanier;
    }

    @BeforeEach
    public void initTest() {
        elementPanier = createEntity(em);
    }

    @Test
    @Transactional
    void createElementPanier() throws Exception {
        int databaseSizeBeforeCreate = elementPanierRepository.findAll().size();
        // Create the ElementPanier
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);
        restElementPanierMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeCreate + 1);
        ElementPanier testElementPanier = elementPanierList.get(elementPanierList.size() - 1);
        assertThat(testElementPanier.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testElementPanier.getPrix()).isEqualTo(DEFAULT_PRIX);
        assertThat(testElementPanier.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
    }

    @Test
    @Transactional
    void createElementPanierWithExistingId() throws Exception {
        // Create the ElementPanier with an existing ID
        elementPanier.setId(1L);
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        int databaseSizeBeforeCreate = elementPanierRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restElementPanierMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = elementPanierRepository.findAll().size();
        // set the field null
        elementPanier.setNom(null);

        // Create the ElementPanier, which fails.
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        restElementPanierMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPrixIsRequired() throws Exception {
        int databaseSizeBeforeTest = elementPanierRepository.findAll().size();
        // set the field null
        elementPanier.setPrix(null);

        // Create the ElementPanier, which fails.
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        restElementPanierMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkQuantityIsRequired() throws Exception {
        int databaseSizeBeforeTest = elementPanierRepository.findAll().size();
        // set the field null
        elementPanier.setQuantity(null);

        // Create the ElementPanier, which fails.
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        restElementPanierMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllElementPaniers() throws Exception {
        // Initialize the database
        elementPanierRepository.saveAndFlush(elementPanier);

        // Get all the elementPanierList
        restElementPanierMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(elementPanier.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prix").value(hasItem(DEFAULT_PRIX.doubleValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)));
    }

    @Test
    @Transactional
    void getElementPanier() throws Exception {
        // Initialize the database
        elementPanierRepository.saveAndFlush(elementPanier);

        // Get the elementPanier
        restElementPanierMockMvc
            .perform(get(ENTITY_API_URL_ID, elementPanier.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(elementPanier.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prix").value(DEFAULT_PRIX.doubleValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY));
    }

    @Test
    @Transactional
    void getNonExistingElementPanier() throws Exception {
        // Get the elementPanier
        restElementPanierMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingElementPanier() throws Exception {
        // Initialize the database
        elementPanierRepository.saveAndFlush(elementPanier);

        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();

        // Update the elementPanier
        ElementPanier updatedElementPanier = elementPanierRepository.findById(elementPanier.getId()).get();
        // Disconnect from session so that the updates on updatedElementPanier are not directly saved in db
        em.detach(updatedElementPanier);
        updatedElementPanier.nom(UPDATED_NOM).prix(UPDATED_PRIX).quantity(UPDATED_QUANTITY);
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(updatedElementPanier);

        restElementPanierMockMvc
            .perform(
                put(ENTITY_API_URL_ID, elementPanierDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isOk());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
        ElementPanier testElementPanier = elementPanierList.get(elementPanierList.size() - 1);
        assertThat(testElementPanier.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testElementPanier.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testElementPanier.getQuantity()).isEqualTo(UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    void putNonExistingElementPanier() throws Exception {
        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();
        elementPanier.setId(count.incrementAndGet());

        // Create the ElementPanier
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restElementPanierMockMvc
            .perform(
                put(ENTITY_API_URL_ID, elementPanierDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchElementPanier() throws Exception {
        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();
        elementPanier.setId(count.incrementAndGet());

        // Create the ElementPanier
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restElementPanierMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamElementPanier() throws Exception {
        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();
        elementPanier.setId(count.incrementAndGet());

        // Create the ElementPanier
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restElementPanierMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateElementPanierWithPatch() throws Exception {
        // Initialize the database
        elementPanierRepository.saveAndFlush(elementPanier);

        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();

        // Update the elementPanier using partial update
        ElementPanier partialUpdatedElementPanier = new ElementPanier();
        partialUpdatedElementPanier.setId(elementPanier.getId());

        partialUpdatedElementPanier.prix(UPDATED_PRIX);

        restElementPanierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedElementPanier.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedElementPanier))
            )
            .andExpect(status().isOk());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
        ElementPanier testElementPanier = elementPanierList.get(elementPanierList.size() - 1);
        assertThat(testElementPanier.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testElementPanier.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testElementPanier.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
    }

    @Test
    @Transactional
    void fullUpdateElementPanierWithPatch() throws Exception {
        // Initialize the database
        elementPanierRepository.saveAndFlush(elementPanier);

        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();

        // Update the elementPanier using partial update
        ElementPanier partialUpdatedElementPanier = new ElementPanier();
        partialUpdatedElementPanier.setId(elementPanier.getId());

        partialUpdatedElementPanier.nom(UPDATED_NOM).prix(UPDATED_PRIX).quantity(UPDATED_QUANTITY);

        restElementPanierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedElementPanier.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedElementPanier))
            )
            .andExpect(status().isOk());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
        ElementPanier testElementPanier = elementPanierList.get(elementPanierList.size() - 1);
        assertThat(testElementPanier.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testElementPanier.getPrix()).isEqualTo(UPDATED_PRIX);
        assertThat(testElementPanier.getQuantity()).isEqualTo(UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    void patchNonExistingElementPanier() throws Exception {
        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();
        elementPanier.setId(count.incrementAndGet());

        // Create the ElementPanier
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restElementPanierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, elementPanierDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchElementPanier() throws Exception {
        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();
        elementPanier.setId(count.incrementAndGet());

        // Create the ElementPanier
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restElementPanierMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamElementPanier() throws Exception {
        int databaseSizeBeforeUpdate = elementPanierRepository.findAll().size();
        elementPanier.setId(count.incrementAndGet());

        // Create the ElementPanier
        ElementPanierDTO elementPanierDTO = elementPanierMapper.toDto(elementPanier);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restElementPanierMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(elementPanierDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ElementPanier in the database
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteElementPanier() throws Exception {
        // Initialize the database
        elementPanierRepository.saveAndFlush(elementPanier);

        int databaseSizeBeforeDelete = elementPanierRepository.findAll().size();

        // Delete the elementPanier
        restElementPanierMockMvc
            .perform(delete(ENTITY_API_URL_ID, elementPanier.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ElementPanier> elementPanierList = elementPanierRepository.findAll();
        assertThat(elementPanierList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
