package info4.gl.blog.service;

import info4.gl.blog.service.dto.CooperativeLocalDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link info4.gl.blog.domain.CooperativeLocal}.
 */
public interface CooperativeLocalService {
    /**
     * Save a cooperativeLocal.
     *
     * @param cooperativeLocalDTO the entity to save.
     * @return the persisted entity.
     */
    CooperativeLocalDTO save(CooperativeLocalDTO cooperativeLocalDTO);

    /**
     * Updates a cooperativeLocal.
     *
     * @param cooperativeLocalDTO the entity to update.
     * @return the persisted entity.
     */
    CooperativeLocalDTO update(CooperativeLocalDTO cooperativeLocalDTO);

    /**
     * Partially updates a cooperativeLocal.
     *
     * @param cooperativeLocalDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CooperativeLocalDTO> partialUpdate(CooperativeLocalDTO cooperativeLocalDTO);

    /**
     * Get all the cooperativeLocals.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CooperativeLocalDTO> findAll(Pageable pageable);

    /**
     * Get the "id" cooperativeLocal.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CooperativeLocalDTO> findOne(Long id);

    /**
     * Delete the "id" cooperativeLocal.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
