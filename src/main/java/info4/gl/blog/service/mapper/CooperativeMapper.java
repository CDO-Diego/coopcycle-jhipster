package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Cooperative;
import info4.gl.blog.domain.CooperativeLocal;
import info4.gl.blog.domain.Membre;
import info4.gl.blog.service.dto.CooperativeDTO;
import info4.gl.blog.service.dto.CooperativeLocalDTO;
import info4.gl.blog.service.dto.MembreDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Cooperative} and its DTO {@link CooperativeDTO}.
 */
@Mapper(componentModel = "spring")
public interface CooperativeMapper extends EntityMapper<CooperativeDTO, Cooperative> {
    @Mapping(target = "cooperativeLocals", source = "cooperativeLocals", qualifiedByName = "cooperativeLocalId")
    @Mapping(target = "membres", source = "membres", qualifiedByName = "membreId")
    CooperativeDTO toDto(Cooperative s);

    @Named("cooperativeLocalId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CooperativeLocalDTO toDtoCooperativeLocalId(CooperativeLocal cooperativeLocal);

    @Named("membreId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MembreDTO toDtoMembreId(Membre membre);
}
