package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Livraison;
import info4.gl.blog.domain.Membre;
import info4.gl.blog.domain.Panier;
import info4.gl.blog.service.dto.LivraisonDTO;
import info4.gl.blog.service.dto.MembreDTO;
import info4.gl.blog.service.dto.PanierDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Livraison} and its DTO {@link LivraisonDTO}.
 */
@Mapper(componentModel = "spring")
public interface LivraisonMapper extends EntityMapper<LivraisonDTO, Livraison> {
    @Mapping(target = "panier", source = "panier", qualifiedByName = "panierId")
    @Mapping(target = "livraisonMan", source = "livraisonMan", qualifiedByName = "membreId")
    LivraisonDTO toDto(Livraison s);

    @Named("panierId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PanierDTO toDtoPanierId(Panier panier);

    @Named("membreId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MembreDTO toDtoMembreId(Membre membre);
}
