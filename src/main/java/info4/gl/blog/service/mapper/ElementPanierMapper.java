package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.ElementPanier;
import info4.gl.blog.domain.Panier;
import info4.gl.blog.service.dto.ElementPanierDTO;
import info4.gl.blog.service.dto.PanierDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ElementPanier} and its DTO {@link ElementPanierDTO}.
 */
@Mapper(componentModel = "spring")
public interface ElementPanierMapper extends EntityMapper<ElementPanierDTO, ElementPanier> {
    @Mapping(target = "panier", source = "panier", qualifiedByName = "panierId")
    ElementPanierDTO toDto(ElementPanier s);

    @Named("panierId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PanierDTO toDtoPanierId(Panier panier);
}
