package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.CooperativeLocal;
import info4.gl.blog.domain.Restaurant;
import info4.gl.blog.service.dto.CooperativeLocalDTO;
import info4.gl.blog.service.dto.RestaurantDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link CooperativeLocal} and its DTO {@link CooperativeLocalDTO}.
 */
@Mapper(componentModel = "spring")
public interface CooperativeLocalMapper extends EntityMapper<CooperativeLocalDTO, CooperativeLocal> {
    @Mapping(target = "restaurants", source = "restaurants", qualifiedByName = "restaurantId")
    CooperativeLocalDTO toDto(CooperativeLocal s);

    @Named("restaurantId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    RestaurantDTO toDtoRestaurantId(Restaurant restaurant);
}
