package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Membre;
import info4.gl.blog.domain.Paiement;
import info4.gl.blog.domain.Panier;
import info4.gl.blog.domain.Restaurant;
import info4.gl.blog.service.dto.MembreDTO;
import info4.gl.blog.service.dto.PaiementDTO;
import info4.gl.blog.service.dto.PanierDTO;
import info4.gl.blog.service.dto.RestaurantDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Panier} and its DTO {@link PanierDTO}.
 */
@Mapper(componentModel = "spring")
public interface PanierMapper extends EntityMapper<PanierDTO, Panier> {
    @Mapping(target = "paiement", source = "paiement", qualifiedByName = "paiementId")
    @Mapping(target = "restaurant", source = "restaurant", qualifiedByName = "restaurantId")
    @Mapping(target = "client", source = "client", qualifiedByName = "membreId")
    PanierDTO toDto(Panier s);

    @Named("paiementId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PaiementDTO toDtoPaiementId(Paiement paiement);

    @Named("restaurantId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    RestaurantDTO toDtoRestaurantId(Restaurant restaurant);

    @Named("membreId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MembreDTO toDtoMembreId(Membre membre);
}
