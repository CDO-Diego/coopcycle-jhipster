package info4.gl.blog.service.mapper;

import info4.gl.blog.domain.Membre;
import info4.gl.blog.service.dto.MembreDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Membre} and its DTO {@link MembreDTO}.
 */
@Mapper(componentModel = "spring")
public interface MembreMapper extends EntityMapper<MembreDTO, Membre> {}
