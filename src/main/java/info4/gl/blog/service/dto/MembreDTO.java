package info4.gl.blog.service.dto;

import info4.gl.blog.domain.enumeration.MembreType;
import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link info4.gl.blog.domain.Membre} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class MembreDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;

    @NotNull
    private String prenom;

    @NotNull
    private MembreType type;

    @NotNull
    private String adresse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public MembreType getType() {
        return type;
    }

    public void setType(MembreType type) {
        this.type = type;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MembreDTO)) {
            return false;
        }

        MembreDTO membreDTO = (MembreDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, membreDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MembreDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", type='" + getType() + "'" +
            ", adresse='" + getAdresse() + "'" +
            "}";
    }
}
