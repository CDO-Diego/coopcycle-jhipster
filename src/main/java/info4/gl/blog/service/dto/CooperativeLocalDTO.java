package info4.gl.blog.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link info4.gl.blog.domain.CooperativeLocal} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CooperativeLocalDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;

    @NotNull
    private String localisation;

    private RestaurantDTO restaurants;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocalisation() {
        return localisation;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public RestaurantDTO getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(RestaurantDTO restaurants) {
        this.restaurants = restaurants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CooperativeLocalDTO)) {
            return false;
        }

        CooperativeLocalDTO cooperativeLocalDTO = (CooperativeLocalDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, cooperativeLocalDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CooperativeLocalDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", localisation='" + getLocalisation() + "'" +
            ", restaurants=" + getRestaurants() +
            "}";
    }
}
