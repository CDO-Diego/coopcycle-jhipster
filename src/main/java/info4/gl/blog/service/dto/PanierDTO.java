package info4.gl.blog.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link info4.gl.blog.domain.Panier} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PanierDTO implements Serializable {

    private Long id;

    private Instant heureLivraison;

    private PaiementDTO paiement;

    private RestaurantDTO restaurant;

    private MembreDTO client;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getHeureLivraison() {
        return heureLivraison;
    }

    public void setHeureLivraison(Instant heureLivraison) {
        this.heureLivraison = heureLivraison;
    }

    public PaiementDTO getPaiement() {
        return paiement;
    }

    public void setPaiement(PaiementDTO paiement) {
        this.paiement = paiement;
    }

    public RestaurantDTO getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantDTO restaurant) {
        this.restaurant = restaurant;
    }

    public MembreDTO getClient() {
        return client;
    }

    public void setClient(MembreDTO client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PanierDTO)) {
            return false;
        }

        PanierDTO panierDTO = (PanierDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, panierDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PanierDTO{" +
            "id=" + getId() +
            ", heureLivraison='" + getHeureLivraison() + "'" +
            ", paiement=" + getPaiement() +
            ", restaurant=" + getRestaurant() +
            ", client=" + getClient() +
            "}";
    }
}
