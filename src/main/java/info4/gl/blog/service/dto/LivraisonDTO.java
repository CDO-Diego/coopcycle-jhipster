package info4.gl.blog.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link info4.gl.blog.domain.Livraison} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class LivraisonDTO implements Serializable {

    private Long id;

    private PanierDTO panier;

    private MembreDTO livraisonMan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PanierDTO getPanier() {
        return panier;
    }

    public void setPanier(PanierDTO panier) {
        this.panier = panier;
    }

    public MembreDTO getLivraisonMan() {
        return livraisonMan;
    }

    public void setLivraisonMan(MembreDTO livraisonMan) {
        this.livraisonMan = livraisonMan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LivraisonDTO)) {
            return false;
        }

        LivraisonDTO livraisonDTO = (LivraisonDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, livraisonDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LivraisonDTO{" +
            "id=" + getId() +
            ", panier=" + getPanier() +
            ", livraisonMan=" + getLivraisonMan() +
            "}";
    }
}
