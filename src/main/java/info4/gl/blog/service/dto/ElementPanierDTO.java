package info4.gl.blog.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link info4.gl.blog.domain.ElementPanier} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ElementPanierDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;

    @NotNull
    private Float prix;

    @NotNull
    private Integer quantity;

    private PanierDTO panier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public PanierDTO getPanier() {
        return panier;
    }

    public void setPanier(PanierDTO panier) {
        this.panier = panier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ElementPanierDTO)) {
            return false;
        }

        ElementPanierDTO elementPanierDTO = (ElementPanierDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, elementPanierDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ElementPanierDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prix=" + getPrix() +
            ", quantity=" + getQuantity() +
            ", panier=" + getPanier() +
            "}";
    }
}
