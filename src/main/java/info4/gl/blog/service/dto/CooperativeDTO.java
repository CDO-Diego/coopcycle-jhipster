package info4.gl.blog.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link info4.gl.blog.domain.Cooperative} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CooperativeDTO implements Serializable {

    private Long id;

    @NotNull
    private String nom;

    private CooperativeLocalDTO cooperativeLocals;

    private MembreDTO membres;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public CooperativeLocalDTO getCooperativeLocals() {
        return cooperativeLocals;
    }

    public void setCooperativeLocals(CooperativeLocalDTO cooperativeLocals) {
        this.cooperativeLocals = cooperativeLocals;
    }

    public MembreDTO getMembres() {
        return membres;
    }

    public void setMembres(MembreDTO membres) {
        this.membres = membres;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CooperativeDTO)) {
            return false;
        }

        CooperativeDTO cooperativeDTO = (CooperativeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, cooperativeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CooperativeDTO{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", cooperativeLocals=" + getCooperativeLocals() +
            ", membres=" + getMembres() +
            "}";
    }
}
