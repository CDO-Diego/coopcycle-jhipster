package info4.gl.blog.service;

import info4.gl.blog.service.dto.MembreDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link info4.gl.blog.domain.Membre}.
 */
public interface MembreService {
    /**
     * Save a membre.
     *
     * @param membreDTO the entity to save.
     * @return the persisted entity.
     */
    MembreDTO save(MembreDTO membreDTO);

    /**
     * Updates a membre.
     *
     * @param membreDTO the entity to update.
     * @return the persisted entity.
     */
    MembreDTO update(MembreDTO membreDTO);

    /**
     * Partially updates a membre.
     *
     * @param membreDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MembreDTO> partialUpdate(MembreDTO membreDTO);

    /**
     * Get all the membres.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MembreDTO> findAll(Pageable pageable);

    /**
     * Get the "id" membre.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MembreDTO> findOne(Long id);

    /**
     * Delete the "id" membre.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
