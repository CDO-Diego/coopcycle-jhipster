package info4.gl.blog.service;

import info4.gl.blog.service.dto.ElementPanierDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link info4.gl.blog.domain.ElementPanier}.
 */
public interface ElementPanierService {
    /**
     * Save a elementPanier.
     *
     * @param elementPanierDTO the entity to save.
     * @return the persisted entity.
     */
    ElementPanierDTO save(ElementPanierDTO elementPanierDTO);

    /**
     * Updates a elementPanier.
     *
     * @param elementPanierDTO the entity to update.
     * @return the persisted entity.
     */
    ElementPanierDTO update(ElementPanierDTO elementPanierDTO);

    /**
     * Partially updates a elementPanier.
     *
     * @param elementPanierDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ElementPanierDTO> partialUpdate(ElementPanierDTO elementPanierDTO);

    /**
     * Get all the elementPaniers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ElementPanierDTO> findAll(Pageable pageable);

    /**
     * Get the "id" elementPanier.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ElementPanierDTO> findOne(Long id);

    /**
     * Delete the "id" elementPanier.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
