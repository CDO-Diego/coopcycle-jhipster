package info4.gl.blog.service.impl;

import info4.gl.blog.domain.ElementPanier;
import info4.gl.blog.repository.ElementPanierRepository;
import info4.gl.blog.service.ElementPanierService;
import info4.gl.blog.service.dto.ElementPanierDTO;
import info4.gl.blog.service.mapper.ElementPanierMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ElementPanier}.
 */
@Service
@Transactional
public class ElementPanierServiceImpl implements ElementPanierService {

    private final Logger log = LoggerFactory.getLogger(ElementPanierServiceImpl.class);

    private final ElementPanierRepository elementPanierRepository;

    private final ElementPanierMapper elementPanierMapper;

    public ElementPanierServiceImpl(ElementPanierRepository elementPanierRepository, ElementPanierMapper elementPanierMapper) {
        this.elementPanierRepository = elementPanierRepository;
        this.elementPanierMapper = elementPanierMapper;
    }

    @Override
    public ElementPanierDTO save(ElementPanierDTO elementPanierDTO) {
        log.debug("Request to save ElementPanier : {}", elementPanierDTO);
        ElementPanier elementPanier = elementPanierMapper.toEntity(elementPanierDTO);
        elementPanier = elementPanierRepository.save(elementPanier);
        return elementPanierMapper.toDto(elementPanier);
    }

    @Override
    public ElementPanierDTO update(ElementPanierDTO elementPanierDTO) {
        log.debug("Request to update ElementPanier : {}", elementPanierDTO);
        ElementPanier elementPanier = elementPanierMapper.toEntity(elementPanierDTO);
        elementPanier = elementPanierRepository.save(elementPanier);
        return elementPanierMapper.toDto(elementPanier);
    }

    @Override
    public Optional<ElementPanierDTO> partialUpdate(ElementPanierDTO elementPanierDTO) {
        log.debug("Request to partially update ElementPanier : {}", elementPanierDTO);

        return elementPanierRepository
            .findById(elementPanierDTO.getId())
            .map(existingElementPanier -> {
                elementPanierMapper.partialUpdate(existingElementPanier, elementPanierDTO);

                return existingElementPanier;
            })
            .map(elementPanierRepository::save)
            .map(elementPanierMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ElementPanierDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ElementPaniers");
        return elementPanierRepository.findAll(pageable).map(elementPanierMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ElementPanierDTO> findOne(Long id) {
        log.debug("Request to get ElementPanier : {}", id);
        return elementPanierRepository.findById(id).map(elementPanierMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ElementPanier : {}", id);
        elementPanierRepository.deleteById(id);
    }
}
