package info4.gl.blog.service.impl;

import info4.gl.blog.domain.Membre;
import info4.gl.blog.repository.MembreRepository;
import info4.gl.blog.service.MembreService;
import info4.gl.blog.service.dto.MembreDTO;
import info4.gl.blog.service.mapper.MembreMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Membre}.
 */
@Service
@Transactional
public class MembreServiceImpl implements MembreService {

    private final Logger log = LoggerFactory.getLogger(MembreServiceImpl.class);

    private final MembreRepository membreRepository;

    private final MembreMapper membreMapper;

    public MembreServiceImpl(MembreRepository membreRepository, MembreMapper membreMapper) {
        this.membreRepository = membreRepository;
        this.membreMapper = membreMapper;
    }

    @Override
    public MembreDTO save(MembreDTO membreDTO) {
        log.debug("Request to save Membre : {}", membreDTO);
        Membre membre = membreMapper.toEntity(membreDTO);
        membre = membreRepository.save(membre);
        return membreMapper.toDto(membre);
    }

    @Override
    public MembreDTO update(MembreDTO membreDTO) {
        log.debug("Request to update Membre : {}", membreDTO);
        Membre membre = membreMapper.toEntity(membreDTO);
        membre = membreRepository.save(membre);
        return membreMapper.toDto(membre);
    }

    @Override
    public Optional<MembreDTO> partialUpdate(MembreDTO membreDTO) {
        log.debug("Request to partially update Membre : {}", membreDTO);

        return membreRepository
            .findById(membreDTO.getId())
            .map(existingMembre -> {
                membreMapper.partialUpdate(existingMembre, membreDTO);

                return existingMembre;
            })
            .map(membreRepository::save)
            .map(membreMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MembreDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Membres");
        return membreRepository.findAll(pageable).map(membreMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MembreDTO> findOne(Long id) {
        log.debug("Request to get Membre : {}", id);
        return membreRepository.findById(id).map(membreMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Membre : {}", id);
        membreRepository.deleteById(id);
    }
}
