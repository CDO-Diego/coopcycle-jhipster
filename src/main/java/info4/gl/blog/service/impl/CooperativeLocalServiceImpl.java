package info4.gl.blog.service.impl;

import info4.gl.blog.domain.CooperativeLocal;
import info4.gl.blog.repository.CooperativeLocalRepository;
import info4.gl.blog.service.CooperativeLocalService;
import info4.gl.blog.service.dto.CooperativeLocalDTO;
import info4.gl.blog.service.mapper.CooperativeLocalMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CooperativeLocal}.
 */
@Service
@Transactional
public class CooperativeLocalServiceImpl implements CooperativeLocalService {

    private final Logger log = LoggerFactory.getLogger(CooperativeLocalServiceImpl.class);

    private final CooperativeLocalRepository cooperativeLocalRepository;

    private final CooperativeLocalMapper cooperativeLocalMapper;

    public CooperativeLocalServiceImpl(
        CooperativeLocalRepository cooperativeLocalRepository,
        CooperativeLocalMapper cooperativeLocalMapper
    ) {
        this.cooperativeLocalRepository = cooperativeLocalRepository;
        this.cooperativeLocalMapper = cooperativeLocalMapper;
    }

    @Override
    public CooperativeLocalDTO save(CooperativeLocalDTO cooperativeLocalDTO) {
        log.debug("Request to save CooperativeLocal : {}", cooperativeLocalDTO);
        CooperativeLocal cooperativeLocal = cooperativeLocalMapper.toEntity(cooperativeLocalDTO);
        cooperativeLocal = cooperativeLocalRepository.save(cooperativeLocal);
        return cooperativeLocalMapper.toDto(cooperativeLocal);
    }

    @Override
    public CooperativeLocalDTO update(CooperativeLocalDTO cooperativeLocalDTO) {
        log.debug("Request to update CooperativeLocal : {}", cooperativeLocalDTO);
        CooperativeLocal cooperativeLocal = cooperativeLocalMapper.toEntity(cooperativeLocalDTO);
        cooperativeLocal = cooperativeLocalRepository.save(cooperativeLocal);
        return cooperativeLocalMapper.toDto(cooperativeLocal);
    }

    @Override
    public Optional<CooperativeLocalDTO> partialUpdate(CooperativeLocalDTO cooperativeLocalDTO) {
        log.debug("Request to partially update CooperativeLocal : {}", cooperativeLocalDTO);

        return cooperativeLocalRepository
            .findById(cooperativeLocalDTO.getId())
            .map(existingCooperativeLocal -> {
                cooperativeLocalMapper.partialUpdate(existingCooperativeLocal, cooperativeLocalDTO);

                return existingCooperativeLocal;
            })
            .map(cooperativeLocalRepository::save)
            .map(cooperativeLocalMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CooperativeLocalDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CooperativeLocals");
        return cooperativeLocalRepository.findAll(pageable).map(cooperativeLocalMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CooperativeLocalDTO> findOne(Long id) {
        log.debug("Request to get CooperativeLocal : {}", id);
        return cooperativeLocalRepository.findById(id).map(cooperativeLocalMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CooperativeLocal : {}", id);
        cooperativeLocalRepository.deleteById(id);
    }
}
