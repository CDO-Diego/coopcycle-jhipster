package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import info4.gl.blog.domain.enumeration.MembreType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Membre.
 */
@Entity
@Table(name = "membre")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Membre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "prenom", nullable = false)
    private String prenom;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private MembreType type;

    @NotNull
    @Column(name = "adresse", nullable = false)
    private String adresse;

    @OneToMany(mappedBy = "membres")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "cooperativeLocals", "membres" }, allowSetters = true)
    private Set<Cooperative> cooperatives = new HashSet<>();

    @OneToMany(mappedBy = "client")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "paiement", "restaurant", "client", "livraisons" }, allowSetters = true)
    private Set<Panier> paniers = new HashSet<>();

    @OneToMany(mappedBy = "livraisonMan")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "panier", "livraisonMan" }, allowSetters = true)
    private Set<Livraison> deliveries = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Membre id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Membre nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Membre prenom(String prenom) {
        this.setPrenom(prenom);
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public MembreType getType() {
        return this.type;
    }

    public Membre type(MembreType type) {
        this.setType(type);
        return this;
    }

    public void setType(MembreType type) {
        this.type = type;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public Membre adresse(String adresse) {
        this.setAdresse(adresse);
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Set<Cooperative> getCooperatives() {
        return this.cooperatives;
    }

    public void setCooperatives(Set<Cooperative> cooperatives) {
        if (this.cooperatives != null) {
            this.cooperatives.forEach(i -> i.setMembres(null));
        }
        if (cooperatives != null) {
            cooperatives.forEach(i -> i.setMembres(this));
        }
        this.cooperatives = cooperatives;
    }

    public Membre cooperatives(Set<Cooperative> cooperatives) {
        this.setCooperatives(cooperatives);
        return this;
    }

    public Membre addCooperative(Cooperative cooperative) {
        this.cooperatives.add(cooperative);
        cooperative.setMembres(this);
        return this;
    }

    public Membre removeCooperative(Cooperative cooperative) {
        this.cooperatives.remove(cooperative);
        cooperative.setMembres(null);
        return this;
    }

    public Set<Panier> getPaniers() {
        return this.paniers;
    }

    public void setPaniers(Set<Panier> paniers) {
        if (this.paniers != null) {
            this.paniers.forEach(i -> i.setClient(null));
        }
        if (paniers != null) {
            paniers.forEach(i -> i.setClient(this));
        }
        this.paniers = paniers;
    }

    public Membre paniers(Set<Panier> paniers) {
        this.setPaniers(paniers);
        return this;
    }

    public Membre addPaniers(Panier panier) {
        this.paniers.add(panier);
        panier.setClient(this);
        return this;
    }

    public Membre removePaniers(Panier panier) {
        this.paniers.remove(panier);
        panier.setClient(null);
        return this;
    }

    public Set<Livraison> getDeliveries() {
        return this.deliveries;
    }

    public void setDeliveries(Set<Livraison> livraisons) {
        if (this.deliveries != null) {
            this.deliveries.forEach(i -> i.setLivraisonMan(null));
        }
        if (livraisons != null) {
            livraisons.forEach(i -> i.setLivraisonMan(this));
        }
        this.deliveries = livraisons;
    }

    public Membre deliveries(Set<Livraison> livraisons) {
        this.setDeliveries(livraisons);
        return this;
    }

    public Membre addDeliveries(Livraison livraison) {
        this.deliveries.add(livraison);
        livraison.setLivraisonMan(this);
        return this;
    }

    public Membre removeDeliveries(Livraison livraison) {
        this.deliveries.remove(livraison);
        livraison.setLivraisonMan(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Membre)) {
            return false;
        }
        return id != null && id.equals(((Membre) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Membre{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", type='" + getType() + "'" +
            ", adresse='" + getAdresse() + "'" +
            "}";
    }
}
