package info4.gl.blog.domain.enumeration;

/**
 * The MembreType enumeration.
 */
public enum MembreType {
    ARTISANT,
    LIVREUR,
    CLIENT,
}
