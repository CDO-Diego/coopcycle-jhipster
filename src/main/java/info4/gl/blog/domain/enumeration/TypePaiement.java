package info4.gl.blog.domain.enumeration;

/**
 * The TypePaiement enumeration.
 */
public enum TypePaiement {
    CB,
    MASTERCARD,
    VISA,
    PAYPAL,
    APPLEPAY,
    GOOGLEPAY,
    ChequeRepas,
    BITCOIN,
    IZLY,
}
