package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CooperativeLocal.
 */
@Entity
@Table(name = "cooperative_local")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CooperativeLocal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "localisation", nullable = false)
    private String localisation;

    @ManyToOne
    @JsonIgnoreProperties(value = { "cooperatives", "paniers" }, allowSetters = true)
    private Restaurant restaurants;

    @OneToMany(mappedBy = "cooperativeLocals")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "cooperativeLocals", "membres" }, allowSetters = true)
    private Set<Cooperative> cooperatives = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CooperativeLocal id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public CooperativeLocal nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLocalisation() {
        return this.localisation;
    }

    public CooperativeLocal localisation(String localisation) {
        this.setLocalisation(localisation);
        return this;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public Restaurant getRestaurants() {
        return this.restaurants;
    }

    public void setRestaurants(Restaurant restaurant) {
        this.restaurants = restaurant;
    }

    public CooperativeLocal restaurants(Restaurant restaurant) {
        this.setRestaurants(restaurant);
        return this;
    }

    public Set<Cooperative> getCooperatives() {
        return this.cooperatives;
    }

    public void setCooperatives(Set<Cooperative> cooperatives) {
        if (this.cooperatives != null) {
            this.cooperatives.forEach(i -> i.setCooperativeLocals(null));
        }
        if (cooperatives != null) {
            cooperatives.forEach(i -> i.setCooperativeLocals(this));
        }
        this.cooperatives = cooperatives;
    }

    public CooperativeLocal cooperatives(Set<Cooperative> cooperatives) {
        this.setCooperatives(cooperatives);
        return this;
    }

    public CooperativeLocal addCooperative(Cooperative cooperative) {
        this.cooperatives.add(cooperative);
        cooperative.setCooperativeLocals(this);
        return this;
    }

    public CooperativeLocal removeCooperative(Cooperative cooperative) {
        this.cooperatives.remove(cooperative);
        cooperative.setCooperativeLocals(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CooperativeLocal)) {
            return false;
        }
        return id != null && id.equals(((CooperativeLocal) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CooperativeLocal{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", localisation='" + getLocalisation() + "'" +
            "}";
    }
}
