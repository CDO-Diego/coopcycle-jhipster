package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Restaurant.
 */
@Entity
@Table(name = "restaurant")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Restaurant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "localisation", nullable = false)
    private String localisation;

    @OneToMany(mappedBy = "restaurants")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "restaurants", "cooperatives" }, allowSetters = true)
    private Set<CooperativeLocal> cooperatives = new HashSet<>();

    @OneToMany(mappedBy = "restaurant")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "paiement", "restaurant", "client", "livraisons" }, allowSetters = true)
    private Set<Panier> paniers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Restaurant id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Restaurant nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return this.description;
    }

    public Restaurant description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocalisation() {
        return this.localisation;
    }

    public Restaurant localisation(String localisation) {
        this.setLocalisation(localisation);
        return this;
    }

    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }

    public Set<CooperativeLocal> getCooperatives() {
        return this.cooperatives;
    }

    public void setCooperatives(Set<CooperativeLocal> cooperativeLocals) {
        if (this.cooperatives != null) {
            this.cooperatives.forEach(i -> i.setRestaurants(null));
        }
        if (cooperativeLocals != null) {
            cooperativeLocals.forEach(i -> i.setRestaurants(this));
        }
        this.cooperatives = cooperativeLocals;
    }

    public Restaurant cooperatives(Set<CooperativeLocal> cooperativeLocals) {
        this.setCooperatives(cooperativeLocals);
        return this;
    }

    public Restaurant addCooperative(CooperativeLocal cooperativeLocal) {
        this.cooperatives.add(cooperativeLocal);
        cooperativeLocal.setRestaurants(this);
        return this;
    }

    public Restaurant removeCooperative(CooperativeLocal cooperativeLocal) {
        this.cooperatives.remove(cooperativeLocal);
        cooperativeLocal.setRestaurants(null);
        return this;
    }

    public Set<Panier> getPaniers() {
        return this.paniers;
    }

    public void setPaniers(Set<Panier> paniers) {
        if (this.paniers != null) {
            this.paniers.forEach(i -> i.setRestaurant(null));
        }
        if (paniers != null) {
            paniers.forEach(i -> i.setRestaurant(this));
        }
        this.paniers = paniers;
    }

    public Restaurant paniers(Set<Panier> paniers) {
        this.setPaniers(paniers);
        return this;
    }

    public Restaurant addPaniers(Panier panier) {
        this.paniers.add(panier);
        panier.setRestaurant(this);
        return this;
    }

    public Restaurant removePaniers(Panier panier) {
        this.paniers.remove(panier);
        panier.setRestaurant(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Restaurant)) {
            return false;
        }
        return id != null && id.equals(((Restaurant) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Restaurant{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", description='" + getDescription() + "'" +
            ", localisation='" + getLocalisation() + "'" +
            "}";
    }
}
