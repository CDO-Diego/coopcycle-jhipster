package info4.gl.blog.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Panier.
 */
@Entity
@Table(name = "panier")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Panier implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "heure_livraison")
    private Instant heureLivraison;

    @OneToOne
    @JoinColumn(unique = true)
    private Paiement paiement;

    @ManyToOne
    @JsonIgnoreProperties(value = { "cooperatives", "paniers" }, allowSetters = true)
    private Restaurant restaurant;

    @ManyToOne
    @JsonIgnoreProperties(value = { "cooperatives", "paniers", "deliveries" }, allowSetters = true)
    private Membre client;

    @OneToMany(mappedBy = "panier")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "panier", "livraisonMan" }, allowSetters = true)
    private Set<Livraison> livraisons = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Panier id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getHeureLivraison() {
        return this.heureLivraison;
    }

    public Panier heureLivraison(Instant heureLivraison) {
        this.setHeureLivraison(heureLivraison);
        return this;
    }

    public void setHeureLivraison(Instant heureLivraison) {
        this.heureLivraison = heureLivraison;
    }

    public Paiement getPaiement() {
        return this.paiement;
    }

    public void setPaiement(Paiement paiement) {
        this.paiement = paiement;
    }

    public Panier paiement(Paiement paiement) {
        this.setPaiement(paiement);
        return this;
    }

    public Restaurant getRestaurant() {
        return this.restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Panier restaurant(Restaurant restaurant) {
        this.setRestaurant(restaurant);
        return this;
    }

    public Membre getClient() {
        return this.client;
    }

    public void setClient(Membre membre) {
        this.client = membre;
    }

    public Panier client(Membre membre) {
        this.setClient(membre);
        return this;
    }

    public Set<Livraison> getLivraisons() {
        return this.livraisons;
    }

    public void setLivraisons(Set<Livraison> livraisons) {
        if (this.livraisons != null) {
            this.livraisons.forEach(i -> i.setPanier(null));
        }
        if (livraisons != null) {
            livraisons.forEach(i -> i.setPanier(this));
        }
        this.livraisons = livraisons;
    }

    public Panier livraisons(Set<Livraison> livraisons) {
        this.setLivraisons(livraisons);
        return this;
    }

    public Panier addLivraison(Livraison livraison) {
        this.livraisons.add(livraison);
        livraison.setPanier(this);
        return this;
    }

    public Panier removeLivraison(Livraison livraison) {
        this.livraisons.remove(livraison);
        livraison.setPanier(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Panier)) {
            return false;
        }
        return id != null && id.equals(((Panier) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Panier{" +
            "id=" + getId() +
            ", heureLivraison='" + getHeureLivraison() + "'" +
            "}";
    }
}
