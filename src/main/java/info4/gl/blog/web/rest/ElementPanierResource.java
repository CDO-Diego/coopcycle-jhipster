package info4.gl.blog.web.rest;

import info4.gl.blog.repository.ElementPanierRepository;
import info4.gl.blog.service.ElementPanierService;
import info4.gl.blog.service.dto.ElementPanierDTO;
import info4.gl.blog.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link info4.gl.blog.domain.ElementPanier}.
 */
@RestController
@RequestMapping("/api")
public class ElementPanierResource {

    private final Logger log = LoggerFactory.getLogger(ElementPanierResource.class);

    private static final String ENTITY_NAME = "elementPanier";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ElementPanierService elementPanierService;

    private final ElementPanierRepository elementPanierRepository;

    public ElementPanierResource(ElementPanierService elementPanierService, ElementPanierRepository elementPanierRepository) {
        this.elementPanierService = elementPanierService;
        this.elementPanierRepository = elementPanierRepository;
    }

    /**
     * {@code POST  /element-paniers} : Create a new elementPanier.
     *
     * @param elementPanierDTO the elementPanierDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new elementPanierDTO, or with status {@code 400 (Bad Request)} if the elementPanier has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/element-paniers")
    public ResponseEntity<ElementPanierDTO> createElementPanier(@Valid @RequestBody ElementPanierDTO elementPanierDTO)
        throws URISyntaxException {
        log.debug("REST request to save ElementPanier : {}", elementPanierDTO);
        if (elementPanierDTO.getId() != null) {
            throw new BadRequestAlertException("A new elementPanier cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ElementPanierDTO result = elementPanierService.save(elementPanierDTO);
        return ResponseEntity
            .created(new URI("/api/element-paniers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /element-paniers/:id} : Updates an existing elementPanier.
     *
     * @param id the id of the elementPanierDTO to save.
     * @param elementPanierDTO the elementPanierDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated elementPanierDTO,
     * or with status {@code 400 (Bad Request)} if the elementPanierDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the elementPanierDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/element-paniers/{id}")
    public ResponseEntity<ElementPanierDTO> updateElementPanier(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ElementPanierDTO elementPanierDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ElementPanier : {}, {}", id, elementPanierDTO);
        if (elementPanierDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, elementPanierDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!elementPanierRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ElementPanierDTO result = elementPanierService.update(elementPanierDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, elementPanierDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /element-paniers/:id} : Partial updates given fields of an existing elementPanier, field will ignore if it is null
     *
     * @param id the id of the elementPanierDTO to save.
     * @param elementPanierDTO the elementPanierDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated elementPanierDTO,
     * or with status {@code 400 (Bad Request)} if the elementPanierDTO is not valid,
     * or with status {@code 404 (Not Found)} if the elementPanierDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the elementPanierDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/element-paniers/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ElementPanierDTO> partialUpdateElementPanier(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ElementPanierDTO elementPanierDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ElementPanier partially : {}, {}", id, elementPanierDTO);
        if (elementPanierDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, elementPanierDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!elementPanierRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ElementPanierDTO> result = elementPanierService.partialUpdate(elementPanierDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, elementPanierDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /element-paniers} : get all the elementPaniers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of elementPaniers in body.
     */
    @GetMapping("/element-paniers")
    public ResponseEntity<List<ElementPanierDTO>> getAllElementPaniers(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ElementPaniers");
        Page<ElementPanierDTO> page = elementPanierService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /element-paniers/:id} : get the "id" elementPanier.
     *
     * @param id the id of the elementPanierDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the elementPanierDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/element-paniers/{id}")
    public ResponseEntity<ElementPanierDTO> getElementPanier(@PathVariable Long id) {
        log.debug("REST request to get ElementPanier : {}", id);
        Optional<ElementPanierDTO> elementPanierDTO = elementPanierService.findOne(id);
        return ResponseUtil.wrapOrNotFound(elementPanierDTO);
    }

    /**
     * {@code DELETE  /element-paniers/:id} : delete the "id" elementPanier.
     *
     * @param id the id of the elementPanierDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/element-paniers/{id}")
    public ResponseEntity<Void> deleteElementPanier(@PathVariable Long id) {
        log.debug("REST request to delete ElementPanier : {}", id);
        elementPanierService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
