package info4.gl.blog.repository;

import info4.gl.blog.domain.ElementPanier;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ElementPanier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ElementPanierRepository extends JpaRepository<ElementPanier, Long> {}
