import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import PaiementService from '@/entities/paiement/paiement.service';
import { IPaiement } from '@/shared/model/paiement.model';

import RestaurantService from '@/entities/restaurant/restaurant.service';
import { IRestaurant } from '@/shared/model/restaurant.model';

import MembreService from '@/entities/membre/membre.service';
import { IMembre } from '@/shared/model/membre.model';

import LivraisonService from '@/entities/livraison/livraison.service';
import { ILivraison } from '@/shared/model/livraison.model';

import { IPanier, Panier } from '@/shared/model/panier.model';
import PanierService from './panier.service';

const validations: any = {
  panier: {
    heureLivraison: {},
  },
};

@Component({
  validations,
})
export default class PanierUpdate extends Vue {
  @Inject('panierService') private panierService: () => PanierService;
  @Inject('alertService') private alertService: () => AlertService;

  public panier: IPanier = new Panier();

  @Inject('paiementService') private paiementService: () => PaiementService;

  public paiements: IPaiement[] = [];

  @Inject('restaurantService') private restaurantService: () => RestaurantService;

  public restaurants: IRestaurant[] = [];

  @Inject('membreService') private membreService: () => MembreService;

  public membres: IMembre[] = [];

  @Inject('livraisonService') private livraisonService: () => LivraisonService;

  public livraisons: ILivraison[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.panierId) {
        vm.retrievePanier(to.params.panierId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.panier.id) {
      this.panierService()
        .update(this.panier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.panier.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.panierService()
        .create(this.panier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.panier.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.panier[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.panier[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.panier[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.panier[field] = null;
    }
  }

  public retrievePanier(panierId): void {
    this.panierService()
      .find(panierId)
      .then(res => {
        res.heureLivraison = new Date(res.heureLivraison);
        this.panier = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.paiementService()
      .retrieve()
      .then(res => {
        this.paiements = res.data;
      });
    this.restaurantService()
      .retrieve()
      .then(res => {
        this.restaurants = res.data;
      });
    this.membreService()
      .retrieve()
      .then(res => {
        this.membres = res.data;
      });
    this.livraisonService()
      .retrieve()
      .then(res => {
        this.livraisons = res.data;
      });
  }
}
