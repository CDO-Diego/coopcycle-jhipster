import { Component, Vue, Inject } from 'vue-property-decorator';

import { ICooperativeLocal } from '@/shared/model/cooperative-local.model';
import CooperativeLocalService from './cooperative-local.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class CooperativeLocalDetails extends Vue {
  @Inject('cooperativeLocalService') private cooperativeLocalService: () => CooperativeLocalService;
  @Inject('alertService') private alertService: () => AlertService;

  public cooperativeLocal: ICooperativeLocal = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.cooperativeLocalId) {
        vm.retrieveCooperativeLocal(to.params.cooperativeLocalId);
      }
    });
  }

  public retrieveCooperativeLocal(cooperativeLocalId) {
    this.cooperativeLocalService()
      .find(cooperativeLocalId)
      .then(res => {
        this.cooperativeLocal = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
