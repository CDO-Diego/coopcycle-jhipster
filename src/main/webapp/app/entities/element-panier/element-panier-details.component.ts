import { Component, Vue, Inject } from 'vue-property-decorator';

import { IElementPanier } from '@/shared/model/element-panier.model';
import ElementPanierService from './element-panier.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ElementPanierDetails extends Vue {
  @Inject('elementPanierService') private elementPanierService: () => ElementPanierService;
  @Inject('alertService') private alertService: () => AlertService;

  public elementPanier: IElementPanier = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.elementPanierId) {
        vm.retrieveElementPanier(to.params.elementPanierId);
      }
    });
  }

  public retrieveElementPanier(elementPanierId) {
    this.elementPanierService()
      .find(elementPanierId)
      .then(res => {
        this.elementPanier = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
