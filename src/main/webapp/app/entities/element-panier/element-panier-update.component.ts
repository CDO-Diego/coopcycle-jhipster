import { Component, Vue, Inject } from 'vue-property-decorator';

import { required, decimal, numeric } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import PanierService from '@/entities/panier/panier.service';
import { IPanier } from '@/shared/model/panier.model';

import { IElementPanier, ElementPanier } from '@/shared/model/element-panier.model';
import ElementPanierService from './element-panier.service';

const validations: any = {
  elementPanier: {
    nom: {
      required,
    },
    prix: {
      required,
      decimal,
    },
    quantity: {
      required,
      numeric,
    },
  },
};

@Component({
  validations,
})
export default class ElementPanierUpdate extends Vue {
  @Inject('elementPanierService') private elementPanierService: () => ElementPanierService;
  @Inject('alertService') private alertService: () => AlertService;

  public elementPanier: IElementPanier = new ElementPanier();

  @Inject('panierService') private panierService: () => PanierService;

  public paniers: IPanier[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.elementPanierId) {
        vm.retrieveElementPanier(to.params.elementPanierId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.elementPanier.id) {
      this.elementPanierService()
        .update(this.elementPanier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.elementPanier.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.elementPanierService()
        .create(this.elementPanier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.elementPanier.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveElementPanier(elementPanierId): void {
    this.elementPanierService()
      .find(elementPanierId)
      .then(res => {
        this.elementPanier = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.panierService()
      .retrieve()
      .then(res => {
        this.paniers = res.data;
      });
  }
}
