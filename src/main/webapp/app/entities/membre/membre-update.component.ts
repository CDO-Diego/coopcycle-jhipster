import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import CooperativeService from '@/entities/cooperative/cooperative.service';
import { ICooperative } from '@/shared/model/cooperative.model';

import PanierService from '@/entities/panier/panier.service';
import { IPanier } from '@/shared/model/panier.model';

import LivraisonService from '@/entities/livraison/livraison.service';
import { ILivraison } from '@/shared/model/livraison.model';

import { IMembre, Membre } from '@/shared/model/membre.model';
import MembreService from './membre.service';
import { MembreType } from '@/shared/model/enumerations/membre-type.model';

const validations: any = {
  membre: {
    nom: {
      required,
    },
    prenom: {
      required,
    },
    type: {
      required,
    },
    adresse: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class MembreUpdate extends Vue {
  @Inject('membreService') private membreService: () => MembreService;
  @Inject('alertService') private alertService: () => AlertService;

  public membre: IMembre = new Membre();

  @Inject('cooperativeService') private cooperativeService: () => CooperativeService;

  public cooperatives: ICooperative[] = [];

  @Inject('panierService') private panierService: () => PanierService;

  public paniers: IPanier[] = [];

  @Inject('livraisonService') private livraisonService: () => LivraisonService;

  public livraisons: ILivraison[] = [];
  public membreTypeValues: string[] = Object.keys(MembreType);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.membreId) {
        vm.retrieveMembre(to.params.membreId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.membre.id) {
      this.membreService()
        .update(this.membre)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.membre.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.membreService()
        .create(this.membre)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('blogApp.membre.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveMembre(membreId): void {
    this.membreService()
      .find(membreId)
      .then(res => {
        this.membre = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.cooperativeService()
      .retrieve()
      .then(res => {
        this.cooperatives = res.data;
      });
    this.panierService()
      .retrieve()
      .then(res => {
        this.paniers = res.data;
      });
    this.livraisonService()
      .retrieve()
      .then(res => {
        this.livraisons = res.data;
      });
  }
}
