import { Component, Vue, Inject } from 'vue-property-decorator';

import { IMembre } from '@/shared/model/membre.model';
import MembreService from './membre.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class MembreDetails extends Vue {
  @Inject('membreService') private membreService: () => MembreService;
  @Inject('alertService') private alertService: () => AlertService;

  public membre: IMembre = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.membreId) {
        vm.retrieveMembre(to.params.membreId);
      }
    });
  }

  public retrieveMembre(membreId) {
    this.membreService()
      .find(membreId)
      .then(res => {
        this.membre = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
