import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import CooperativeService from './cooperative/cooperative.service';
import MembreService from './membre/membre.service';
import CooperativeLocalService from './cooperative-local/cooperative-local.service';
import RestaurantService from './restaurant/restaurant.service';
import PanierService from './panier/panier.service';
import ElementPanierService from './element-panier/element-panier.service';
import PaiementService from './paiement/paiement.service';
import LivraisonService from './livraison/livraison.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('cooperativeService') private cooperativeService = () => new CooperativeService();
  @Provide('membreService') private membreService = () => new MembreService();
  @Provide('cooperativeLocalService') private cooperativeLocalService = () => new CooperativeLocalService();
  @Provide('restaurantService') private restaurantService = () => new RestaurantService();
  @Provide('panierService') private panierService = () => new PanierService();
  @Provide('elementPanierService') private elementPanierService = () => new ElementPanierService();
  @Provide('paiementService') private paiementService = () => new PaiementService();
  @Provide('livraisonService') private livraisonService = () => new LivraisonService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
