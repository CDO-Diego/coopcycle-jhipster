import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

// prettier-ignore
const Cooperative = () => import('@/entities/cooperative/cooperative.vue');
// prettier-ignore
const CooperativeUpdate = () => import('@/entities/cooperative/cooperative-update.vue');
// prettier-ignore
const CooperativeDetails = () => import('@/entities/cooperative/cooperative-details.vue');
// prettier-ignore
const Membre = () => import('@/entities/membre/membre.vue');
// prettier-ignore
const MembreUpdate = () => import('@/entities/membre/membre-update.vue');
// prettier-ignore
const MembreDetails = () => import('@/entities/membre/membre-details.vue');
// prettier-ignore
const CooperativeLocal = () => import('@/entities/cooperative-local/cooperative-local.vue');
// prettier-ignore
const CooperativeLocalUpdate = () => import('@/entities/cooperative-local/cooperative-local-update.vue');
// prettier-ignore
const CooperativeLocalDetails = () => import('@/entities/cooperative-local/cooperative-local-details.vue');
// prettier-ignore
const Restaurant = () => import('@/entities/restaurant/restaurant.vue');
// prettier-ignore
const RestaurantUpdate = () => import('@/entities/restaurant/restaurant-update.vue');
// prettier-ignore
const RestaurantDetails = () => import('@/entities/restaurant/restaurant-details.vue');
// prettier-ignore
const Panier = () => import('@/entities/panier/panier.vue');
// prettier-ignore
const PanierUpdate = () => import('@/entities/panier/panier-update.vue');
// prettier-ignore
const PanierDetails = () => import('@/entities/panier/panier-details.vue');
// prettier-ignore
const ElementPanier = () => import('@/entities/element-panier/element-panier.vue');
// prettier-ignore
const ElementPanierUpdate = () => import('@/entities/element-panier/element-panier-update.vue');
// prettier-ignore
const ElementPanierDetails = () => import('@/entities/element-panier/element-panier-details.vue');
// prettier-ignore
const Paiement = () => import('@/entities/paiement/paiement.vue');
// prettier-ignore
const PaiementUpdate = () => import('@/entities/paiement/paiement-update.vue');
// prettier-ignore
const PaiementDetails = () => import('@/entities/paiement/paiement-details.vue');
// prettier-ignore
const Livraison = () => import('@/entities/livraison/livraison.vue');
// prettier-ignore
const LivraisonUpdate = () => import('@/entities/livraison/livraison-update.vue');
// prettier-ignore
const LivraisonDetails = () => import('@/entities/livraison/livraison-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'cooperative',
      name: 'Cooperative',
      component: Cooperative,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/new',
      name: 'CooperativeCreate',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/edit',
      name: 'CooperativeEdit',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/view',
      name: 'CooperativeView',
      component: CooperativeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'membre',
      name: 'Membre',
      component: Membre,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'membre/new',
      name: 'MembreCreate',
      component: MembreUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'membre/:membreId/edit',
      name: 'MembreEdit',
      component: MembreUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'membre/:membreId/view',
      name: 'MembreView',
      component: MembreDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative-local',
      name: 'CooperativeLocal',
      component: CooperativeLocal,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative-local/new',
      name: 'CooperativeLocalCreate',
      component: CooperativeLocalUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative-local/:cooperativeLocalId/edit',
      name: 'CooperativeLocalEdit',
      component: CooperativeLocalUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative-local/:cooperativeLocalId/view',
      name: 'CooperativeLocalView',
      component: CooperativeLocalDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant',
      name: 'Restaurant',
      component: Restaurant,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/new',
      name: 'RestaurantCreate',
      component: RestaurantUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/:restaurantId/edit',
      name: 'RestaurantEdit',
      component: RestaurantUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'restaurant/:restaurantId/view',
      name: 'RestaurantView',
      component: RestaurantDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'panier',
      name: 'Panier',
      component: Panier,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'panier/new',
      name: 'PanierCreate',
      component: PanierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'panier/:panierId/edit',
      name: 'PanierEdit',
      component: PanierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'panier/:panierId/view',
      name: 'PanierView',
      component: PanierDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'element-panier',
      name: 'ElementPanier',
      component: ElementPanier,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'element-panier/new',
      name: 'ElementPanierCreate',
      component: ElementPanierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'element-panier/:elementPanierId/edit',
      name: 'ElementPanierEdit',
      component: ElementPanierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'element-panier/:elementPanierId/view',
      name: 'ElementPanierView',
      component: ElementPanierDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement',
      name: 'Paiement',
      component: Paiement,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/new',
      name: 'PaiementCreate',
      component: PaiementUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/:paiementId/edit',
      name: 'PaiementEdit',
      component: PaiementUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/:paiementId/view',
      name: 'PaiementView',
      component: PaiementDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'livraison',
      name: 'Livraison',
      component: Livraison,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'livraison/new',
      name: 'LivraisonCreate',
      component: LivraisonUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'livraison/:livraisonId/edit',
      name: 'LivraisonEdit',
      component: LivraisonUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'livraison/:livraisonId/view',
      name: 'LivraisonView',
      component: LivraisonDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
