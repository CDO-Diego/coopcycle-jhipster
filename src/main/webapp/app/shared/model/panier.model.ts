import { IPaiement } from '@/shared/model/paiement.model';
import { IRestaurant } from '@/shared/model/restaurant.model';
import { IMembre } from '@/shared/model/membre.model';
import { ILivraison } from '@/shared/model/livraison.model';

export interface IPanier {
  id?: number;
  heureLivraison?: Date | null;
  paiement?: IPaiement | null;
  restaurant?: IRestaurant | null;
  client?: IMembre | null;
  livraisons?: ILivraison[] | null;
}

export class Panier implements IPanier {
  constructor(
    public id?: number,
    public heureLivraison?: Date | null,
    public paiement?: IPaiement | null,
    public restaurant?: IRestaurant | null,
    public client?: IMembre | null,
    public livraisons?: ILivraison[] | null
  ) {}
}
