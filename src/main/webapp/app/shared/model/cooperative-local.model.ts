import { IRestaurant } from '@/shared/model/restaurant.model';
import { ICooperative } from '@/shared/model/cooperative.model';

export interface ICooperativeLocal {
  id?: number;
  nom?: string;
  localisation?: string;
  restaurants?: IRestaurant | null;
  cooperatives?: ICooperative[] | null;
}

export class CooperativeLocal implements ICooperativeLocal {
  constructor(
    public id?: number,
    public nom?: string,
    public localisation?: string,
    public restaurants?: IRestaurant | null,
    public cooperatives?: ICooperative[] | null
  ) {}
}
