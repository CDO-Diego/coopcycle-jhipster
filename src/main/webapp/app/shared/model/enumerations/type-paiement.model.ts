export enum TypePaiement {
  CB = 'CB',

  MASTERCARD = 'MASTERCARD',

  VISA = 'VISA',

  PAYPAL = 'PAYPAL',

  APPLEPAY = 'APPLEPAY',

  GOOGLEPAY = 'GOOGLEPAY',

  ChequeRepas = 'ChequeRepas',

  BITCOIN = 'BITCOIN',

  IZLY = 'IZLY',
}
