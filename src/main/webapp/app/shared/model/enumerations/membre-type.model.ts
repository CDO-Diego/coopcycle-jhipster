export enum MembreType {
  ARTISANT = 'ARTISANT',

  LIVREUR = 'LIVREUR',

  CLIENT = 'CLIENT',
}
