import { ICooperativeLocal } from '@/shared/model/cooperative-local.model';
import { IMembre } from '@/shared/model/membre.model';

export interface ICooperative {
  id?: number;
  nom?: string;
  cooperativeLocals?: ICooperativeLocal | null;
  membres?: IMembre | null;
}

export class Cooperative implements ICooperative {
  constructor(
    public id?: number,
    public nom?: string,
    public cooperativeLocals?: ICooperativeLocal | null,
    public membres?: IMembre | null
  ) {}
}
