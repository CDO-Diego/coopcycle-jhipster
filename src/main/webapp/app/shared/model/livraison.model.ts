import { IPanier } from '@/shared/model/panier.model';
import { IMembre } from '@/shared/model/membre.model';

export interface ILivraison {
  id?: number;
  panier?: IPanier | null;
  livraisonMan?: IMembre | null;
}

export class Livraison implements ILivraison {
  constructor(public id?: number, public panier?: IPanier | null, public livraisonMan?: IMembre | null) {}
}
