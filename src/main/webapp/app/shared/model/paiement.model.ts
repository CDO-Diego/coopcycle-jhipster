import { TypePaiement } from '@/shared/model/enumerations/type-paiement.model';
export interface IPaiement {
  id?: number;
  type?: TypePaiement;
  quantite?: number;
}

export class Paiement implements IPaiement {
  constructor(public id?: number, public type?: TypePaiement, public quantite?: number) {}
}
