import { ICooperativeLocal } from '@/shared/model/cooperative-local.model';
import { IPanier } from '@/shared/model/panier.model';

export interface IRestaurant {
  id?: number;
  nom?: string;
  description?: string | null;
  localisation?: string;
  cooperatives?: ICooperativeLocal[] | null;
  paniers?: IPanier[] | null;
}

export class Restaurant implements IRestaurant {
  constructor(
    public id?: number,
    public nom?: string,
    public description?: string | null,
    public localisation?: string,
    public cooperatives?: ICooperativeLocal[] | null,
    public paniers?: IPanier[] | null
  ) {}
}
