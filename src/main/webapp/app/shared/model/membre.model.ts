import { ICooperative } from '@/shared/model/cooperative.model';
import { IPanier } from '@/shared/model/panier.model';
import { ILivraison } from '@/shared/model/livraison.model';

import { MembreType } from '@/shared/model/enumerations/membre-type.model';
export interface IMembre {
  id?: number;
  nom?: string;
  prenom?: string;
  type?: MembreType;
  adresse?: string;
  cooperatives?: ICooperative[] | null;
  paniers?: IPanier[] | null;
  deliveries?: ILivraison[] | null;
}

export class Membre implements IMembre {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public type?: MembreType,
    public adresse?: string,
    public cooperatives?: ICooperative[] | null,
    public paniers?: IPanier[] | null,
    public deliveries?: ILivraison[] | null
  ) {}
}
