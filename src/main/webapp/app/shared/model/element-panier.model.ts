import { IPanier } from '@/shared/model/panier.model';

export interface IElementPanier {
  id?: number;
  nom?: string;
  prix?: number;
  quantity?: number;
  panier?: IPanier | null;
}

export class ElementPanier implements IElementPanier {
  constructor(public id?: number, public nom?: string, public prix?: number, public quantity?: number, public panier?: IPanier | null) {}
}
